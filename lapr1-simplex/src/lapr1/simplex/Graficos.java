package lapr1.simplex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Formatter;

/**
 * Contém todos os métodos para a criação dos gráficos de cada problema.
 */
public class Graficos {

    /**
     * Método principal que gera gráfico a partir da informação fornecida pelas
     * equações do problema e das soluções das iterações
     *
     * @param tipo maximização ou minimização
     * @param output nome do ficheiro de output
     * @param formato tipo de ficheiro de output
     * @param func função a maximizar/minimizar
     * @param eqs restrições
     * @param solcs soluçoes intermédias e final
     * @param iteracao numero de iterações
     * @throws IOException
     * @throws InterruptedException
     */
    public static void gerarGrafico(String tipo, String output, String formato, double[] func, double[][] eqs, double[][] solcs, int iteracao) throws IOException, InterruptedException {
        String tipoTexto = "Maximização";
        if (tipo.equalsIgnoreCase("min")) {
            tipoTexto = "Minimização";
        }
        String titulo = String.format("%s de Z = (%.1f*x1) + (%.1f*x2)", tipoTexto, func[0], func[1]);
        int xMax = obterMax(eqs, 0);
        int yMax = obterMax(eqs, 1);
        File pastaTemp = new File("temp");
        Logger.info("A pasta temporária (/temp) foi criada");
        pastaTemp.mkdir();
        for (int i = 0; i < iteracao; i++) {
            imprimirFicheiro("temp/ponto" + i + ".dat", solcs[i][0] + " " + solcs[i][1]);
        }
        imprimirFicheiro("temp/solucao.dat", solcs[iteracao][0] + " " + solcs[iteracao][1]);
        imprimirFicheiro("temp/script.gp", montarScript(xMax, yMax, output, titulo, eqs, formato, solcs, iteracao, func, tipo));
        correrScript("temp/script.gp");
        apagarTemp(pastaTemp);
    }

    /**
     * Prepara as funções das restrições para o gnuplot
     *
     * @param eq restrição a tratar
     * @param yMax y máximo para o caso das verticais
     * @return string pronta para dar plot no gnuplot
     * @throws FileNotFoundException
     */
    public static String montarFunc(double[] eq, int yMax) throws FileNotFoundException {
        String func = "";
        if (eq[1] != 0) {
            func += String.format("a = %s , b = %s , f(x)", eq[0] / eq[1], eq[2] / eq[1]);
        } else {
            func += String.format("'temp/vertical_%s_%s_%s.dat'", eq[0], eq[1], eq[2]);
            gerarVertical(eq, yMax);
        }
        return func;
    }

    /**
     * Prepara as soluções da função objectiva para o gnuplot
     *
     * @param func função objectiva
     * @param soluc soluções da função objectiva
     * @return string pronta para dar plot no gnuplot
     * @throws FileNotFoundException
     */
    public static String montarSolu(double[] func, double[] soluc) throws FileNotFoundException {
        String retaSoluc = "";
        retaSoluc += String.format("a = %s , b = %s , f(x)", func[0] / func[1], soluc[2] / func[1]);
        return retaSoluc;
    }

    /**
     * Gera ficheiro para as restrições verticais
     *
     * @param eq restrição vertical
     * @param yMax y máximo para gerar o ponto máximo
     * @throws FileNotFoundException
     */
    public static void gerarVertical(double[] eq, int yMax) throws FileNotFoundException {
        String pontos = eq[2] + " 0\n" + eq[2] + " " + yMax;
        imprimirFicheiro("temp/vertical_" + eq[0] + "_" + eq[1] + "_" + eq[2] + ".dat", pontos);
    }

    /**
     * Monta script para executar no gnuplot
     *
     * @param xMax x máximo do gráfico
     * @param yMax y máximo do gráfico
     * @param ficheiro nome do ficheiro do grafico
     * @param titulo titulo do grafico
     * @param eqs restrições
     * @param formato formato do ficheiro
     * @param solcs soluções da função objetivo
     * @param iteracao número de iterações final
     * @param func função objetivo
     * @param process processo max/min
     * @return script montado
     * @throws FileNotFoundException
     */
    public static String montarScript(int xMax, int yMax, String ficheiro, String titulo, double[][] eqs, String formato, double[][] solcs, int iteracao, double[] func, String process) throws FileNotFoundException {
        String script = String.format("f(x)=b-a*x%n");
        String terminal;
        int pointSize = 2;
        switch (formato) {
            case "png":
                terminal = "set terminal png nocrop enhanced font arial 14 size 1400,720";
                break;
            case "txt":
                terminal = "set terminal dumb size 130,40";
                break;
            case "eps":
                terminal = "set terminal eps size 8,3";
                pointSize = 1;
                break;
            default:
                terminal = "set terminal png nocrop enhanced font arial 14 size 1400,720";
                break;
        }
        script += String.format("%s%n", terminal);
        script += String.format("set output '%s.%s'%n", ficheiro, formato);
        script += String.format("set key outside%n");
        script += String.format("set key title 'Legenda'%n");
        script += String.format("set key right top%n");
        script += String.format("set key box 1%n");
        script += String.format("set title '%s'%n", titulo);
        script += String.format("set xrange [0:%s]%n", xMax);
        script += String.format("set yrange [0:%s]%n", yMax);
        script += String.format("set xlabel 'x1'%n");
        script += String.format("set ylabel 'x2'%n");
        script += String.format("plot ");
        script += regAdmiss(eqs, yMax, process);
        for (int i = 0; i < eqs.length; i++) {
            script += String.format(" %s w lines lw 2 t '%.1f*x1 + %.1f*x2 = %.1f',", montarFunc(eqs[i], yMax), eqs[i][0], eqs[i][1], eqs[i][2]);
        }
        for (int i = 0; i < iteracao; i++) {
            script += String.format(" %s w lines lw 1 lc 3 t 'Reta Solução da Iteração %s',", montarSolu(func, solcs[i]), i);
            script += String.format(" 'temp/ponto%s.dat' pointtype 13 pointsize %s lc rgb 'blue' t 'Solução da Iteração %s (%.1f; %.1f)',", i, pointSize, i, solcs[i][0], solcs[i][1]);
        }
        script += String.format(" %s w lines lw 2 lc rgb 'red' t 'Reta Solução Otima',", montarSolu(func, solcs[iteracao]));
        script += String.format(" 'temp/solucao.dat' pointtype 5 pointsize %s lc rgb 'red' t 'Solução Otima Z=%.1f (%.1f; %.1f)',", pointSize, solcs[iteracao][2], solcs[iteracao][0], solcs[iteracao][1]);
        script = script.substring(0, script.length() - 1); // remove a ultima virgula, para o gnuplot não mostrar nenhuma mensagem de erro
        script += String.format("%n");
        return script;
    }

    /**
     * Pinta a região admissivel do problema
     *
     * @param eqs restrições do problema
     * @param yMax x2 máximo
     * @param process processo de min ou max
     * @return String pronta para gnuplot
     * @throws FileNotFoundException
     */
    public static String regAdmiss(double[][] eqs, int yMax, String process) throws FileNotFoundException {
        String script = String.format("0 with filledcurve x2 lc rgb '#dfe5e9' t 'Região Admissível',");
        for (int i = 0; i < eqs.length; i++) {
            String[][] fill = {{"x1", "false"}, {"x2", "false"}, {"y1", "false"}, {"y2", "false"}};
            double produto = eqs[i][0] * eqs[i][1];
            int y1 = 2, y2 = 3, x = 1;
            if (process.equalsIgnoreCase("min")) {
                y1 = 3;
                y2 = 2;
                x = 0;
            }
            if (produto >= 0) {
                fill[y2][1] = "true";
            } else {
                fill[y1][1] = "true";
            }
            fill[x][1] = "true";
            for (int j = 0; j < fill.length; j++) {
                if (fill[j][1].equalsIgnoreCase("true")) {
                    script += String.format(" %s with filledcurve %s lc rgb 'white' notitle,", montarFunc(eqs[i], yMax), fill[j][0]);
                }
            }
        }
        return script;
    }

    /**
     * Imprime ficheiro
     *
     * @param output nome do ficheiro de saida
     * @param conteudo conteúdo do ficheiro de saida
     * @throws FileNotFoundException
     */
    public static void imprimirFicheiro(String output, String conteudo) throws FileNotFoundException {
        Formatter formatFicheiro = new Formatter(new File(output));
        formatFicheiro.format(conteudo);
        formatFicheiro.close();
        Logger.info("Ficheiro " + output + " criado");
    }

    /**
     * Obter ponto máximo do gráfico
     *
     * @param eqs restrições do problema
     * @param tipo 1 ou 2, sendo que 1 é para x1 e 2 para x2
     * @return
     */
    public static int obterMax(double[][] eqs, int tipo) {
        double max = 0;
        double ponto;
        for (int i = 0; i < eqs.length; i++) {
            if (eqs[i][tipo] != 0) {
                ponto = eqs[i][2] / eqs[i][tipo];
                if (ponto > max && ponto > 0) {
                    max = ponto;
                }
            }
        }
        return (int) max + 1;
    }

    /**
     * Corre o script com o gnuplot
     *
     * @param ficheiro nome do script
     * @throws IOException
     * @throws InterruptedException
     */
    public static void correrScript(String ficheiro) throws IOException, InterruptedException {
        try {
            Runtime rt = Runtime.getRuntime();
            Process prcs = rt.exec("gnuplot " + ficheiro);
            //Process prcs = rt.exec("C:\\Program Files\\gnuplot\\bin\\gnuplot.exe " + ficheiro);
            prcs.waitFor();
            Logger.info("O gráfico foi gerado a partir do script " + ficheiro);
        } catch (IOException | InterruptedException e) {
            Logger.erro("Foi impossivel executar o comando \"gnuplot " + ficheiro + "\" verifique se tem o gnuplot instalado");
            Logger.erro(e.getLocalizedMessage());
        }
    }

    /**
     * Apaga conteúdo da pasta temporária e a própria pasta
     *
     * @param pastaTemp pasta temporaria gerada
     */
    public static void apagarTemp(File pastaTemp) {
        String[] entries = pastaTemp.list();
        for (String s : entries) {
            File currentFile = new File(pastaTemp.getPath(), s);
            currentFile.delete();
        }
        pastaTemp.delete();
        Logger.info("Pasta temporária (/temp) apagada");
    }

    /**
     * Monta matriz das restrições
     *
     * @param inputConteudo conteúdo do ficheiro de input
     * @param matriz matriz lida
     * @return matriz das restrições
     * @throws FileNotFoundException
     */
    public static double[][] montarRestricoes(String inputConteudo, double[][] matriz) throws FileNotFoundException {
        double[][] eqs = new double[Leitura.contarRestricoes(inputConteudo)][3];
        for (int i = 0; i < eqs.length; i++) {
            for (int j = 0; j < 2; j++) {
                eqs[i][j] = matriz[i + 1][j];
            }
            eqs[i][2] = matriz[i + 1][matriz[i].length - 1];
        }
        return eqs;
    }

    /**
     * Adiciona solução à matriz das restrições
     *
     * @param matriz matriz lida
     * @param solcs matriz das soluções
     * @param base matriz base
     * @param iteracao iteração atual
     */
    public static void adicionarSols(double[][] matriz, double[][] solcs, String[] base, int iteracao) {
        String[] titulos = {"x1", "x2"};
        for (int i = 0; i < titulos.length; i++) {
            int pos = Escrita.procurarBase(titulos[i], base);
            double valor;
            if (pos == -1) {
                valor = 0;
            } else {
                valor = matriz[pos][matriz[0].length - 1];
            }
            if (titulos[i].equalsIgnoreCase("x1")) {
                solcs[iteracao][0] = valor;
            }
            if (titulos[i].equalsIgnoreCase("x2")) {
                solcs[iteracao][1] = valor;
            }
        }
        solcs[iteracao][2] = matriz[0][matriz[0].length - 1];
    }

}
