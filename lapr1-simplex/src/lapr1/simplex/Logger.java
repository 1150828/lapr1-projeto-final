package lapr1.simplex;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Contém todos os métodos para criar os llogs
 */
public class Logger {

    private static void log(String tipo, String mensagem) throws IOException {
        String log = String.format("[%s] %s: %s%n", dataAtual(), tipo, mensagem);
        File ficheiroLog = new File("consola.log");
        if (!ficheiroLog.exists()) {
            ficheiroLog.createNewFile();
        }
        //true = append linha no final do ficheiro
        FileWriter fileWritter = new FileWriter(ficheiroLog.getName(), true);
        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
        bufferWritter.write(log);
        bufferWritter.close();
    }

    private static String dataAtual() {
        GregorianCalendar gregCal = new GregorianCalendar();
        return String.format("%02d/%02d/%04d %02d:%02d:%02d", gregCal.get(Calendar.DAY_OF_MONTH), gregCal.get(Calendar.MONTH) + 1, gregCal.get(Calendar.YEAR), gregCal.get(Calendar.HOUR_OF_DAY), gregCal.get(Calendar.MINUTE), gregCal.get(Calendar.SECOND));
    }

    /**
     * Gera mensagem de erro no ficheiro log
     *
     * @param mensagem mensagem de erro
     */
    public static void erro(String mensagem) {
        try {
            log("ERRO", mensagem);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    /**
     * Gera mensagem de info no ficheiro log
     *
     * @param mensagem mensagem informativa
     */
    public static void info(String mensagem) {
        try {
            log("INFO", mensagem);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    /**
     * Gera mensagem de aviso no ficheiro log
     *
     * @param mensagem mensagem de aviso
     */
    public static void aviso(String mensagem) {
        try {
            log("AVISO", mensagem);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

}
