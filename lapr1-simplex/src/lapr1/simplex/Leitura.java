package lapr1.simplex;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Contém todos os métodos de leitura do projeto
 */
public class Leitura {

    /**
     * Monta a matriz de dupla entrada a partir do ficheiro
     *
     * @param inputConteudo conteudo do ficheiro de leitura
     * @param process min ou max
     * @return double[][] com a matriz do problema
     * @throws FileNotFoundException caso o ficheiro não exista
     */
    public static double[][] montarMatriz(String inputConteudo, String process) throws FileNotFoundException {
        int eqs = contarRestricoes(inputConteudo) + 1;
        int vars = contarVariaveis(inputConteudo) + 1;
        double[][] matrizLida = new double[eqs][vars];
        if (process.equalsIgnoreCase("min")) {
            int temp = vars;
            vars = eqs;
            eqs = temp;
        }
        double[][] matrizTratada = new double[eqs][vars];
        lerMatriz(matrizLida, inputConteudo);
        if (process.equalsIgnoreCase("min")) {
            moverLinhas(matrizLida);
            transpostaMatriz(matrizLida, matrizTratada);
            for (int i = 0; i < matrizTratada.length - 1; i++) {
                moverLinhas(matrizTratada);
            }
        } else {
            matrizTratada = matrizLida;
        }
        double[][] matrizFinal = new double[eqs][vars + (eqs - 1)];
        preencherFinal(matrizTratada, matrizFinal);
        for (int i = 0; i < matrizFinal[0].length; i++) {
            if (matrizFinal[0][i] > 0) {
                matrizFinal[0][i] = matrizFinal[0][i] * -1;
            }
        }
        return matrizFinal;
    }

    /**
     * Preencher matriz final com a matriz tratada adicionando variaveis de
     * folga
     *
     * @param matrizTratada matriz tratada sem variaveis de folga
     * @param matrizFinal matriz final com variaveis de folga
     */
    public static void preencherFinal(double[][] matrizTratada, double[][] matrizFinal) {
        int variaveis = matrizTratada[0].length - 1;
        for (int i = 0; i < matrizTratada.length; i++) {
            for (int j = 0; j < matrizTratada[0].length - 1; j++) {
                matrizFinal[i][j] = matrizTratada[i][j];
            }
            matrizFinal[i][matrizFinal[0].length - 1] = matrizTratada[i][matrizTratada[0].length - 1];
        }
        for (int i = 1; i < matrizFinal.length; i++) {
            matrizFinal[i][variaveis + i - 1] = 1;
        }
    }

    /**
     * Mover linhas para cima, caso seja a primeira passa para última
     *
     * @param matrizInicial
     */
    public static void moverLinhas(double[][] matrizInicial) {
        double[] linhaTemp = new double[matrizInicial[0].length];
        for (int i = 0; i < linhaTemp.length; i++) {
            linhaTemp[i] = matrizInicial[0][i];
        }
        for (int i = 1; i < matrizInicial.length; i++) {
            for (int j = 0; j < matrizInicial[0].length; j++) {
                matrizInicial[i - 1][j] = matrizInicial[i][j];
            }
        }
        for (int i = 0; i < linhaTemp.length; i++) {
            matrizInicial[matrizInicial.length - 1][i] = linhaTemp[i];
        }
    }

    /**
     * Faz a transposta duma matriz
     *
     * @param matriz matriz para fazer transposta
     * @param matrizTransposta matriz transposta
     */
    public static void transpostaMatriz(double[][] matriz, double[][] matrizTransposta) {
        for (int i = 0; i < matriz[0].length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matrizTransposta[i][j] = matriz[j][i];
            }
        }
    }

    /**
     * Monta a matriz dos titulos
     *
     * @param inputConteudo conteúdo do ficheiro de leitura
     * @param process processo minimização ou maximização
     * @return String[] com os titulos
     * @throws FileNotFoundException caso o ficheiro não exista
     */
    public static String[] titulos(String inputConteudo, String process) throws FileNotFoundException {
        int vars = contarVariaveis(inputConteudo);
        int eqs = contarRestricoes(inputConteudo);
        if (process.equalsIgnoreCase("min")) {
            vars = contarRestricoes(inputConteudo);
            eqs = contarVariaveis(inputConteudo);
        }
        String[] titulos = new String[vars + eqs + 1];
        for (int i = 0; i < vars; i++) {
            int var = i + 1;
            titulos[i] = "x" + var;
        }
        for (int i = vars; i < vars + eqs; i++) {
            int eq = i - vars + 1;
            titulos[i] = "s" + eq;
        }
        titulos[titulos.length - 1] = "b";
        return titulos;
    }

    /**
     * Monta a matriz das bases
     *
     * @param inputConteudo conteúdo do ficheiro de leitura
     * @param process processo minimização ou maximização
     * @return String[] com as bases
     * @throws FileNotFoundException caso o ficheiro não exista
     */
    public static String[] base(String inputConteudo, String process) throws FileNotFoundException {
        int nBases = contarRestricoes(inputConteudo) + 1;
        if (process.equalsIgnoreCase("min")) {
            nBases = contarVariaveis(inputConteudo) + 1;
        }
        String[] bases = new String[nBases];
        bases[0] = "Base";
        for (int i = 1; i < bases.length; i++) {
            bases[i] = "s" + i;
        }
        return bases;
    }

    /**
     * Preenche a matriz a partir da informação do ficheiro
     *
     * @param matriz recebe a matriz vazia já criada com os necessários espaços
     * @param inputConteudo conteúdo do ficheiro de leitura
     * @throws FileNotFoundException caso o ficheiro não exista
     */
    public static void lerMatriz(double[][] matriz, String inputConteudo) throws FileNotFoundException {
        Scanner scan = new Scanner(inputConteudo);
        int contagem = 0;
        while (scan.hasNext()) {
            String linha = scan.nextLine();
            linha = linha.toLowerCase();
            linha = linha.replaceAll("\\s", "");
            linha = linha.replaceAll("﻿", "");
            if (contagem == 0) {
                linha = linha.replaceAll("z=", "");
                String[] valores = linha.split("(?=[+-])");
                preencherMatriz(matriz, contagem, valores);
            } else {
                String[] equacao = linha.split("<=|>=");
                String[] valores = equacao[0].split("(?=[+-])");
                preencherMatriz(matriz, contagem, valores);
                equacao[1] = corrigirValor(equacao[1]);
                matriz[contagem][matriz[contagem].length - 1] = Double.parseDouble(equacao[1]);
            }
            contagem++;
        }
    }

    /**
     * Conta o número de variaveis necessárias no problema do ficheiro lido
     *
     * @param inputConteudo conteúdo do ficheiro de leitura
     * @return número de variaveis necessárias
     * @throws FileNotFoundException caso o ficheiro não exista
     */
    public static int contarVariaveis(String inputConteudo) throws FileNotFoundException {
        Scanner scan = new Scanner(inputConteudo);
        String linha = scan.nextLine();
        linha = linha.toLowerCase();
        return linha.length() - linha.replace("x", "").length();
    }

    /**
     * Conta as restrições do problema do ficheiro lido
     *
     * @param inputConteudo conteúdo do ficheiro de leitura
     * @return número de restrições do problema
     * @throws FileNotFoundException caso o ficheiro não exista
     */
    public static int contarRestricoes(String inputConteudo) throws FileNotFoundException {
        Scanner scan = new Scanner(inputConteudo);
        int contagem = 0;
        while (scan.hasNext()) {
            String linha = scan.nextLine();
            if (!linha.equalsIgnoreCase("") || linha != null) {
                contagem++;
            }
        }
        return contagem - 1;
    }

    /**
     * Corrige valores transformando +x ou x em 1 e -x em -1
     *
     * @param val valor a corrigir
     * @return valor corrigido
     */
    public static String corrigirValor(String val) {
        if (val.equalsIgnoreCase("") || val.equalsIgnoreCase("+") || val == null || val.isEmpty() || val.equalsIgnoreCase("﻿")) {
            return "1";
        } else if (val.equalsIgnoreCase("-")) {
            return "-1";
        } else if (val.contains("/")) {
            String[] fracao = val.split("/");
            double resultadoFracao = (double) (Double.parseDouble(fracao[0]) / Double.parseDouble(fracao[1]));
            return "" + resultadoFracao;
        }
        return val;
    }

    /**
     * Método para preencher a matriz no local correto
     *
     * @param matriz matriz por preencher
     * @param contagem linha a preencher
     * @param valores valor e posição onde inserir
     */
    public static void preencherMatriz(double[][] matriz, int contagem, String[] valores) {
        int start = 0;
        if (valores[0].equalsIgnoreCase("")) {
            start = 1;
        }
        for (int i = start; i < valores.length; i++) {
            String[] inc = valores[i].split("x");
            inc[0] = corrigirValor(inc[0]);
            matriz[contagem][Integer.parseInt(inc[1]) - 1] = Double.parseDouble(inc[0]);
        }
    }

    /**
     * Cria uma string do ficheiro de leitura validando as linhas
     *
     * @param input ficheiro de leitura
     * @return String com conteúdo do ficheiro de leitura
     * @throws FileNotFoundException
     */
    public static String ficheiroMemoria(String input) throws FileNotFoundException {
        File ficheiro = new File(input);
        Scanner leitor = new Scanner(ficheiro);

        String memoriaFicheiro = "";
        int linhas = 0;

        while (leitor.hasNext()) {
            String linha = leitor.nextLine();
            linha = linha.toLowerCase();
            linha = linha.replace('w', 'z');
            linha = linha.replace('y', 'x');
            if (linha.equalsIgnoreCase("") || linha == null) {
                Logger.aviso("Foi encontrada uma linha vazia no ficheiro de entrada");
            }
            if (!linha.equalsIgnoreCase("") && linha != null) {
                linhas++;
                if (!verificarLinha(linha, linhas)) {
                    return "error";
                }
                memoriaFicheiro += String.format("%s%n", linha);
            }
        }
        Logger.info("Ficheiro " + input + " foi lido e guardado em memória");
        return memoriaFicheiro;
    }

    /**
     * Verifica se a linha respeita as respetivas restrições (Verificações de
     * Função Objetiva, de Restrições e de Espaços)
     *
     * @param linha linha a verificar
     * @param linhas número da linha
     * @return boolean caso respeite ou não as verificações
     */
    public static boolean verificarLinha(String linha, int linhas) {

        String verificarFuncao = "(z|Z|w|W)(\\s{1,2})?(=)((\\s{1,2})?(\\+|\\-)?(\\s{1,2})?(\\d*(.\\d{1,2})?)((/)(\\d+(.\\d{1,2})?))*(x|X|y|Y)(\\d+))+";
        String verificarRestricao = "((\\s{1,2})?(\\+|\\-)?(\\s{1,2})?(\\d*(.\\d{1,2})?)((/)(\\d+(.\\d{1,2})?))*(x|X|y|Y)(\\d+)(\\s{1,2})?)+(<|>)=(\\s{1,2})?(\\d+(.\\d{1,2})?)(/(\\d+(.\\d{1,2})?))?(\\s{1,2})?";
        String verificarEspacos = ".*(\\s{3,}).*";

        if (linha.matches(verificarEspacos)) {
            Logger.erro(String.format("Na linha %s do ficheiro lido, '%s' contém mais que 2 espaços entre caractéres", linhas, linha));
        }

        if (linhas == 1) {
            if (!linha.matches(verificarFuncao)) {
                Logger.erro(String.format("Na linha %s do ficheiro lido, '%s' não corresponde a uma função objetivo", linhas, linha));
                return false;
            }
        } else {
            if (!linha.matches(verificarRestricao)) {
                Logger.erro(String.format("Na linha %s do ficheiro lido, '%s' não corresponde a uma restrição", linhas, linha));
                return false;
            }
        }

        return !linha.matches(verificarEspacos);
    }
}
