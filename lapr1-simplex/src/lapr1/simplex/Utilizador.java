package lapr1.simplex;

import java.util.Scanner;

/**
 *
 * @author Miguel
 */
public class Utilizador {

    /**
     * Cria uma pergunta para selecionar o processo
     *
     * @param sugestao sugestao a dar ao utilizador
     * @return decisão (max ou min)
     */
    public static String perguntaProcesso(String sugestao) {
        Scanner input = new Scanner(System.in);
        String resposta;
        do {
            resposta = "";
            System.out.printf("Qual é o processo a utilizar neste problema (max/min)? [%s]: ", sugestao);
            resposta += input.nextLine().toLowerCase();
        } while (!(resposta.equalsIgnoreCase("max") || resposta.equalsIgnoreCase("min") || resposta.equalsIgnoreCase("")));
        if (resposta.equalsIgnoreCase("")) {
            resposta = sugestao;
        }
        return resposta;
    }

    /**
     * Sugestão para o processo a usar para a resolução do problema
     *
     * @param inputConteudo conteúdo do ficheiro de leitura
     * @return sugestão do processo
     */
    public static String sugestProcesso(String inputConteudo) {
        String menorIgual = ".*>=.*";

        Scanner leitor = new Scanner(inputConteudo);

        while (leitor.hasNext()) {
            String linha = leitor.nextLine();
            if (linha.matches(menorIgual)) {
                return "min";
            }
        }
        return "max";
    }

    /**
     * Pergunta se deseja gerar gráfico
     *
     * @return boolean dependendo da decisão
     */
    public static boolean perguntaGrafico() {
        Scanner input = new Scanner(System.in);
        String resposta;
        do {
            resposta = "";
            System.out.printf("Deseja também gerar o gráfico deste problema? (s/N)? [%s]: ", "s");
            resposta += input.nextLine().toLowerCase();
        } while (!(resposta.equalsIgnoreCase("s") || resposta.equalsIgnoreCase("n") || resposta.equalsIgnoreCase("sim") || resposta.equalsIgnoreCase("nao") || resposta.equalsIgnoreCase("")));
        if (resposta.equalsIgnoreCase("")) {
            return true;
        } else if (resposta.toLowerCase().charAt(0) == 's') {
            return true;
        }
        return false;
    }

    /**
     * Gera pergunta para o utilizador decidir o formato do gráfico
     *
     * @param sugestao sugestao a dar ao utilizador
     * @return Formato decidido
     */
    public static String perguntaGraficoTipo(String sugestao) {
        Scanner input = new Scanner(System.in);
        String resposta;
        do {
            resposta = "";
            System.out.printf("Qual o formato do gráfico que pretende (png/txt/eps)? [%s]: ", sugestao);
            resposta += input.nextLine().toLowerCase();
        } while (!(resposta.equalsIgnoreCase("png") || resposta.equalsIgnoreCase("txt") || resposta.equalsIgnoreCase("eps") || resposta.equalsIgnoreCase("")));
        if (resposta.equalsIgnoreCase("")) {
            resposta = sugestao;
        }
        return resposta;
    }

    /**
     * Gera input para o utilizador decidir o nome do gráfico
     *
     * @param sugestao sugestao a dar ao utilizador
     * @param process processo do gráfico
     * @return String com nome decidido
     */
    public static String perguntaGraficoNome(String sugestao, String process) {
        String processo = "";
        if (process.equalsIgnoreCase("min")) {
            processo = " de minimização";
        }
        Scanner input = new Scanner(System.in);
        String resposta;
        resposta = "";
        System.out.printf("Nome a atribuir ao gráfico%s (sem extensão) [%s]: ", processo, sugestao);
        resposta += input.nextLine().toLowerCase();
        if (resposta.equalsIgnoreCase("")) {
            resposta = sugestao;
        }
        return resposta;
    }
}
