package lapr1.simplex;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Contém todos os métodos de teste do projeto
 */
public class Testes {

    /**
     * Verifica se a coluna inserida é a coluna pivot da matriz inserida
     *
     * @param matriz matriz a testar
     * @param coluna resultado a testar
     * @return resultado boolean true ou false
     */
    public static boolean teste_pivotColuna(double[][] matriz, int coluna) {
        return (Simplex.pivotColuna(matriz) == coluna);
    }

    /**
     * Verifica se a linha inserida é a linha pivot da matriz inserida
     *
     * @param matriz matriz a testar
     * @param coluna coluna do pivot a testar
     * @param linha linha do pivot a testar
     * @return resultado boolean true ou false
     */
    public static boolean teste_pivotLinha(double[][] matriz, int coluna, int linha) {
        return (Simplex.pivotLinha(matriz, coluna) == linha);
    }

    /**
     * Compara duas matrizes inseridas
     *
     * @param matriz1 matriz a testar 1
     * @param matriz2 matriz a testar 2
     * @return resultado boolean true ou false dependendo se são iguais ou não
     */
    public static boolean compararMatrizes(double[][] matriz1, double[][] matriz2) {
        if (matriz1.length != matriz2.length || matriz1[0].length != matriz2[0].length) {
            return false;
        }
        for (int i = 0; i < matriz1.length; i++) {
            for (int j = 0; i < matriz1[0].length; i++) {
                if (matriz1[i][j] != matriz2[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Método de teste para o exercicio 1 da ficha
     *
     * @param matriz matriz a testar
     * @return resultado boolean true ou false
     */
    public static boolean teste_montarMatriz(double[][] matriz) {
        double[][] esperada = {{-3, -5, 0, 0, 0}, {1, 0, 4, 1, 0, 0}, {0, 2, 12, 0, 1, 0}, {3, 2, 18, 0, 0, 1}}; //introduzimos manualmente a matriz que se espera obter
        return compararMatrizes(matriz, esperada);
    }

    /**
     * Teste de montagem de iteração
     *
     * @param matriz matriz a testar
     * @param historico variavel de historico
     * @param titulos titulos da tabela
     * @param vars número de variaveis
     * @param bases bases da iteração
     * @param linha linha do pivot
     * @param coluna coluna do pivot
     * @param esperado iteração montada a testar
     * @param process processo de min ou max
     * @return resultado boolean true ou false
     */
    public static boolean teste_montarIteracao(double[][] matriz, String historico, String[] titulos, int vars, String[] bases, int linha, int coluna, String esperado, String process) {
        return (Escrita.montarIteracao(matriz, historico, titulos, vars, bases, linha, coluna, process).equals(esperado));
    }

    /**
     * Teste de montagem de solução
     *
     * @param matriz matriz a testar
     * @param titulos titulos da tabela
     * @param vars numero de variaveis
     * @param bases bases da iteração
     * @param esperado resultado esperado
     * @param process processo de max ou min
     * @return resultado boolean true ou false
     */
    public static boolean teste_montarSolucao(double[][] matriz, String[] titulos, int vars, String[] bases, String esperado, String process) {
        return (Escrita.montarSolucao(matriz, titulos, vars, bases, process).equals(esperado));
    }

    /**
     * Teste de contagem de variaveis
     *
     * @param nomeFicheiro nome do ficheiro a testar
     * @param esperado resultado esperado
     * @return resultado boolean true ou false
     * @throws FileNotFoundException se o ficheiro não for encontrado
     */
    public static boolean teste_contarVariaveis(String nomeFicheiro, int esperado) throws FileNotFoundException {
        return (Leitura.contarVariaveis(nomeFicheiro) == esperado);
    }

    /**
     * Teste de contagem de restrições
     *
     * @param nomeFicheiro nome do ficheiro a testar
     * @param esperado resultado esperado
     * @return resultado boolean true ou false
     * @throws FileNotFoundException se o ficheiro não for encontrado
     */
    public static boolean teste_contarEq(String nomeFicheiro, int esperado) throws FileNotFoundException {
        return (Leitura.contarRestricoes(nomeFicheiro) == esperado);
    }

    /**
     * Teste da class Logger
     *
     * @throws java.io.IOException
     */
    public static void teste_Logger() throws IOException {
        Logger.erro("mensagem de erro");
        Logger.aviso("mensagem de aviso");
        Logger.info("mensagem de informação");
    }
}
