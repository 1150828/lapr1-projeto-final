package lapr1.simplex;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Contém o método que inicia o programa
 */
public class Main {

    /**
     * Inicia todas as varáveis necessárias para o programa e chama todos os
     * métodos necessários para calcular a optmização
     *
     * @param args parametros de entrada
     * @throws FileNotFoundException no caso do ficheiro input não existir
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        if (args.length == 2) {
            String input = args[0];
            String output = args[1];
            String inputConteudo = Leitura.ficheiroMemoria(input);
            if (!inputConteudo.equalsIgnoreCase("error")) {
                introducao();
                String process = Utilizador.perguntaProcesso(Utilizador.sugestProcesso(inputConteudo));
                String[] titulos = Leitura.titulos(inputConteudo, process);
                String[] bases = Leitura.base(inputConteudo, process);
                double[][] matriz = Leitura.montarMatriz(inputConteudo, process);
                double[] func = {matriz[0][0] * -1, matriz[0][1] * -1};
                double[][] eqs = Graficos.montarRestricoes(inputConteudo, matriz);
                double[] func_min = {matriz[1][matriz[0].length - 1], matriz[2][matriz[0].length - 1]};
                double[][] eqs_min = {{matriz[1][0], matriz[2][0], matriz[0][0] * -1}, {matriz[1][1], matriz[2][1], matriz[0][1] * -1}};
                double[][] solcs = new double[99][3];
                int iteracao = 0;
                iteracao = Simplex.optimizarMatriz(inputConteudo, output, iteracao, matriz, titulos, bases, solcs, process);
                if (Leitura.contarVariaveis(inputConteudo) == 2) {
                    if (Utilizador.perguntaGrafico()) {
                        String[] problema = input.split("\\.");
                        Graficos.gerarGrafico("max", Utilizador.perguntaGraficoNome("grafico_max_" + problema[0], "max"), Utilizador.perguntaGraficoTipo("png"), func, eqs, solcs, iteracao);
                        if (process.equalsIgnoreCase("min")) {
                            double[][] solcs_min = {{matriz[0][2], matriz[0][3], matriz[0][4]}, {matriz[0][2], matriz[0][3], matriz[0][4]}};
                            Graficos.gerarGrafico("min", Utilizador.perguntaGraficoNome("grafico_min_" + problema[0], "min"), Utilizador.perguntaGraficoTipo("png"), func_min, eqs_min, solcs_min, 0);
                        }
                    }
                }
            } else {
                System.out.println("O programa foi interrompido devido a um erro no ficheiro de leitura, por favor verifique o ficheiro consola.log");
            }
        } else {
            Logger.aviso("Não foram introduzidos corretamente os argumentos no programa");
            System.out.println("Deverá introduzir os argumentos ao correr o programa.");
            System.out.println("Ex: java -jar lapr1-simplex.jar input.txt output.txt");
        }
    }

    private static void introducao() {
        Logger.info("O programa foi iniciado");
        System.out.println("");
        System.out.println("Programa para resolução de problemas de programação linear.");
        System.out.println("Nota: Tudo que estiver entre parênteses são as possiveis escolhas e");
        System.out.println("e tudo que estiver entre parênteses retos são sugestões do programa,");
        System.out.println("caso queira utilizar a sugestão clique [ENTER]");
        System.out.println("");
    }
}
