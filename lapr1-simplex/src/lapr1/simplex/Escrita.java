package lapr1.simplex;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Contém todos os métodos de escrita do projeto
 */
public class Escrita {

    /**
     * Motar iteração para escrita no ficheiro final
     *
     * @param matriz matriz para montar no ficheiro
     * @param historico conteudo anterior da string historico
     * @param titulos titulos da matriz
     * @param vars numero de variaveis
     * @param bases bases da matriz na iteração atual
     * @param linha número de linhas da matriz
     * @param coluna número de colunas da matriz
     * @param process processo de minimização ou maximização
     * @return novo histórico já com a iteração montada
     */
    public static String montarIteracao(double[][] matriz, String historico, String[] titulos, int vars, String[] bases, int linha, int coluna, String process) {
        historico += montarTitulos(titulos, coluna);
        historico += String.format("%n");
        historico += montarMatriz(matriz, bases, linha);
        historico += String.format("%nValor atual de Z = %.2f", matriz[0][matriz[0].length - 1]);
        historico += montarSolucao(matriz, titulos, vars, bases, process);
        historico += String.format("%n");
        return historico;
    }

    /**
     * Imprime a string do historico de todas as iterações no ficheiro output
     *
     * @param output nome para o ficheiro de output
     * @param historico todas as iterações
     * @throws FileNotFoundException
     */
    public static void imprimirFicheiro(String output, String historico) throws FileNotFoundException {
        File ficheiro = new File(output);
        Formatter formatFicheiro = new Formatter(ficheiro);
        formatFicheiro.format(historico);
        formatFicheiro.close();
        System.out.printf("Ficheiro %s que contém a resolução detalhada do problema foi criado!%n", output);
        Logger.info(String.format("Ficheiro %s que contém a resolução do problema foi criado!%n", output));
    }

    /**
     * Verifica se o titulo procurado está na matriz bases
     *
     * @param titulo titulo a procurar
     * @param bases matriz com as bases
     * @return posição na matriz, caso não exista retorna -1
     */
    public static int procurarBase(String titulo, String[] bases) {
        for (int i = 0; i < bases.length; i++) {
            if (titulo.equalsIgnoreCase(bases[i])) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Monta a linha com os titulos
     *
     * @param titulos matriz dos titulos
     * @param coluna coluna a identificar como pivot
     * @return linha montada
     */
    public static String montarTitulos(String[] titulos, int coluna) {
        String resultado = "";
        for (int i = 0; i < titulos.length; i++) {
            if (i == coluna) {
                resultado += String.format("%7s ", ">" + titulos[i] + "<");
            } else {
                resultado += String.format("%7s ", titulos[i]);
            }
        }
        return resultado;
    }

    /**
     * Monta a matriz numa tabela organizada
     *
     * @param matriz matriz para montar
     * @param bases bases ao lado direito
     * @param linha linha a ser identificada como pivot
     * @return matriz montada
     */
    public static String montarMatriz(double[][] matriz, String[] bases, int linha) {
        String resultado = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                resultado += String.format("%7.2f ", matriz[i][j]);
            }
            resultado += String.format("%7s ", bases[i]);
            if (i == linha) {
                resultado += String.format(" <");
            }
            resultado += String.format("%n");
        }
        return resultado;
    }

    /**
     * Monta a solução da iteração
     *
     * @param matriz matriz atual
     * @param titulos titulos da matriz
     * @param vars numero de variaveis
     * @param bases bases atuais da matriz
     * @param process processo max/min
     * @return solução montada
     */
    public static String montarSolucao(double[][] matriz, String[] titulos, int vars, String[] bases, String process) {
        String resultado = "";
        String tipo = "";
        if (process.equalsIgnoreCase("min")) {
            tipo = " dual";
        }
        resultado += String.format("%nSol. atual%s: (", tipo);
        for (int i = 0; i < titulos.length - 1; i++) {
            resultado += String.format(titulos[i] + "; ");
        }
        resultado = resultado.substring(0, resultado.length() - 2);
        resultado += String.format(") = (");
        for (int i = 0; i < titulos.length - 1; i++) {
            int pos = procurarBase(titulos[i], bases);
            double valor;
            if (pos == -1) {
                valor = 0;
            } else {
                valor = matriz[pos][matriz[0].length - 1];
            }
            resultado += String.format("%.2f; ", valor);
        }
        resultado = resultado.substring(0, resultado.length() - 2);
        resultado += String.format(")%n");
        if (process.equalsIgnoreCase("min")) {
            resultado += String.format("Sol. atual primal: (");
            for (int i = vars; i < matriz[0].length - 1; i++) {
                resultado += String.format("y%s; ", i - vars + 1);
            }
            resultado = resultado.substring(0, resultado.length() - 2);
            resultado += String.format(") = (");
            for (int i = vars; i < matriz[0].length - 1; i++) {
                resultado += String.format("%.2f; ", matriz[0][i]);
            }
            resultado = resultado.substring(0, resultado.length() - 2);
            resultado += String.format(")%n");
        }
        return resultado;
    }

    /**
     * Monta as restrições do problema
     *
     * @param inputConteudo conteudo do ficheiro de input
     * @param process processo max/min
     * @param matriz matriz do problema
     * @return string das equações do problema
     * @throws FileNotFoundException caso o ficheiro não exista
     */
    public static String montarEqs(String inputConteudo, String process, double[][] matriz) throws FileNotFoundException {
        String tipo = "";
        if (process.equalsIgnoreCase("min")) {
            tipo = " Primal";
        }
        String resultado = String.format("Equações do Problema%s:%n", tipo);
        Scanner scan = new Scanner(inputConteudo);
        while (scan.hasNext()) {
            String linha = scan.nextLine();
            linha = linha.replaceAll("\\s", "");
            linha = linha.replaceAll("﻿", "");
            linha = linha.toUpperCase();
            if (process.equalsIgnoreCase("min")) {
                linha = linha.replace('Z', 'W');
                linha = linha.replace('X', 'Y');
            }
            if (!linha.equalsIgnoreCase("") || linha != null) {
                resultado += String.format("%s%n", linha);
            }
        }
        if (process.equalsIgnoreCase("min")) {
            resultado += String.format("%nEquações do Problema Dual:%n");
            resultado += String.format("Z=");
            for (int i = 0; i < matriz.length - 1; i++) {
                resultado += String.format("(%.2fX%s)+", matriz[0][i] * -1, i + 1);
            }
            resultado = resultado.substring(0, resultado.length() - 1);
            resultado += String.format("%n");
            for (int i = 1; i < matriz.length; i++) {
                for (int j = 0; j < matriz.length - 1; j++) {
                    resultado += String.format("(%.2fX%s)+", matriz[i][j], j + 1);
                }
                resultado += String.format("<=%.2f", matriz[i][matriz[0].length - 1]);
                resultado = resultado.substring(0, resultado.length() - 1);
                resultado += String.format("%n");
            }
        }
        resultado += String.format("%n");
        return resultado;
    }

}
