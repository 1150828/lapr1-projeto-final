package lapr1.simplex;

import java.io.FileNotFoundException;

/**
 * Contém todos os métodos de cálculo para o método simplex do projeto
 */
public class Simplex {

    /**
     * Verifica se existe algum negativo na optimização
     *
     * @param matriz matriz a verificar
     * @return true ou false conforme se já chegamos à solução optimizada
     */
    public static boolean verificarOptimizada(double[][] matriz) {
        for (int i = 0; i < matriz[0].length; i++) {
            if (matriz[0][i] < 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Pesquisa a coluna do pivot da matriz
     *
     * @param matriz matriz a verificar
     * @return coluna do pivot
     */
    public static int pivotColuna(double[][] matriz) {
        int pos = 0;
        for (int i = 1; i < matriz[0].length - 1; i++) {
            if (matriz[0][i] < matriz[0][pos]) {
                pos = i;
            }
        }
        return pos;
    }

    /**
     * Pesquisa a linha do pivot da matriz
     *
     * @param matriz matriz a verificar
     * @param coluna coluna do pivot
     * @return linha do pivot
     */
    public static int pivotLinha(double[][] matriz, int coluna) {
        int pos = 1;
        for (int i = 2; i < matriz.length; i++) {
            if (matriz[i][coluna] != 0) {
                if (matriz[i][matriz[i].length - 1] / matriz[i][coluna] >= 0) {
                    if (matriz[i][matriz[i].length - 1] / matriz[i][coluna] < matriz[pos][matriz[pos].length - 1] / matriz[pos][coluna] || matriz[pos][matriz[pos].length - 1] / matriz[pos][coluna] <= 0) {
                        pos = i;
                    }
                }
            }
        }
        return pos;
    }

    /**
     * Subtrai a multiplicação duma linha a outra
     *
     * @param matriz matriz dos cálculos
     * @param linha1 linha que vai ser multiplicada e subtraida
     * @param linha2 linha para subtração
     * @param multiplicarLinha1 - valor que se multiplica a linha 1
     */
    public static void subtrairLinhas(double[][] matriz, int linha1, int linha2, double multiplicarLinha1) {
        for (int i = 0; i < matriz[0].length; i++) {
            matriz[linha2][i] = matriz[linha2][i] - matriz[linha1][i] * multiplicarLinha1;
        }
    }

    /**
     * Multiplica uma linha por um valor
     *
     * @param matriz matriz dos cálculos
     * @param linha linha que vai ser multiplicada
     * @param multiplicarLinha valor para multiplicar
     */
    public static void multiplicarLinha(double[][] matriz, int linha, double multiplicarLinha) {
        for (int i = 0; i < matriz[0].length; i++) {
            matriz[linha][i] = matriz[linha][i] * multiplicarLinha;
        }
    }

    /**
     * Muda as bases da matriz
     *
     * @param titulos titulos da matriz
     * @param bases bases da matriz
     * @param linha linha da base que vai ser alterada
     * @param coluna coluna dos titulos que vai ser adicionada nas bases
     */
    public static void mudarBase(String[] titulos, String[] bases, int linha, int coluna) {
        bases[linha] = titulos[coluna];
    }

    /**
     * Executa os cálculos até a matriz estar optimizada
     *
     * @param inputConteudo conteúdo do ficheiro de entrada
     * @param output nome do ficheiro de saída
     * @param iteracao variável do número de iterações
     * @param matriz matriz a optimizar
     * @param titulos titulos da matriz
     * @param bases base da matriz
     * @param solcs soluções de cada iteração
     * @param process processo de min ou max
     * @return int com número de iterações feitas
     * @throws FileNotFoundException caso os ficheiros não existam ou não
     * consigam ser criados
     */
    public static int optimizarMatriz(String inputConteudo, String output, int iteracao, double[][] matriz, String[] titulos, String[] bases, double[][] solcs, String process) throws FileNotFoundException {
        Graficos.adicionarSols(matriz, solcs, bases, iteracao);
        String historico = Escrita.montarEqs(inputConteudo, process, matriz);
        historico += String.format("Resolução:%nIT. %s:%n%n", iteracao);
        historico = Escrita.montarIteracao(matriz, historico, titulos, Leitura.contarVariaveis(inputConteudo), bases, -1, -1, process);
        while (!Simplex.verificarOptimizada(matriz)) {
            int coluna = Simplex.pivotColuna(matriz);
            int linha = Simplex.pivotLinha(matriz, coluna);
            Simplex.mudarBase(titulos, bases, linha, coluna);
            Simplex.multiplicarLinha(matriz, linha, 1 / matriz[linha][coluna]);
            for (int i = 0; i < matriz.length; i++) {
                if (i != linha) {
                    Simplex.subtrairLinhas(matriz, linha, i, matriz[i][coluna]);
                }
            }
            iteracao++;
            historico += String.format("IT. %s:%n%n", iteracao);
            historico = Escrita.montarIteracao(matriz, historico, titulos, Leitura.contarVariaveis(inputConteudo), bases, linha, coluna, process);
            Graficos.adicionarSols(matriz, solcs, bases, iteracao);
        }
        historico += String.format("A última solução é optima.");
        Escrita.imprimirFicheiro(output, historico);
        return iteracao;
    }
}
